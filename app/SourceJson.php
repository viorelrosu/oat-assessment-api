<?php

namespace App;
use Storage;
use Stichoza\GoogleTranslate\GoogleTranslate;

class SourceJson
{
    public function getDataJson(String $lang)
    {
        $json = Storage::disk('local')->get('questions.json');
        $json = json_decode($json, true);

        $tr = new GoogleTranslate();
        $tr->setTarget($lang);

        $data = collect($json);
        $data = $data->map(function ($item, $key) use ($tr) {
            $question = [
                'text' => $tr->translate($item['text']),
                'createdAt' => $item['createdAt'],
                'choices' => [],
            ];

            $choices = array_map(function ($choice) use ($tr) {
                return [
                    'text' => $tr->translate($choice['text']),
                ];
            }, $item['choices']);

            $question['choices'] = $choices;
            return $question;
        });

        return $data;
    }

    /**
     * insertData - Inserts a new question and associated choices in the storage/app/questions.json file
     *
     * @param  Array $data - Question and associated choices to insert
     * @return object - The question and associated choices (not translated) inserted
     */
    public function insertDataJson(Array $data)
    {
        $data['createdAt'] = date("Y-m-d h:i:s");
        $json = Storage::disk('local')->get('questions.json');
        $json = json_decode($json, true);
        $json[] = $data;
        Storage::disk('local')->put('questions.json', json_encode($json, JSON_UNESCAPED_UNICODE));
        return $data;
    }
}