# OAT Assessment API

OAT Assessment API is an web application API to handle the questions and the choices 

## Cloning Repository

Clone the repository

    git clone https://bitbucket.org/viorelrosu/oat-assessment-api.git

Switch to the repo folder

    cd oat-assessment-api

Install all the dependencies using composer

    composer install

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

----------

# Code overview

## Testing API

Run the laravel development server

    php artisan serve

## Things to know

### Retrieve the list of translated questions and associated choices (GET)

    http://localhost:8000/api/questions

The api requires a String param named lang - language format (ISO-639-1 code) in which the questions and choices should be translated

### Creates a new question and associated choices (POST)

    http://localhost:8000/api/questions

The api requires an object  - the question and associated choices

Example: 

{
    "text": "What is the capital of Luxembourg ?",
    "choices": [
      {
        "text": "Luxembourg"
      },
      {
        "text": "Paris"
      },
      {
        "text": "Berlin"
      }
    ]
  }

### Sources data

The api uses two different data sources. 

- `storage/app/questions.json` - JSON file - contains all the questions
- `storage/app/questions.csv` - CSV file - contains all the questions

Each data source it is used only one at a time and it is configurable in the system

- `config/services.sources` - config services file - indicates the data source and can have different options: json, csv 

The api is easily extendable, in order to do that you need to: 

- 1. Create the classes for the new Data Source
- 1.1 create a class in `app` for the new data source
- 1.1 create a class in `app/Services` as adapter class which implements class `app/Interfaces/Source.php`

- 2. Insert the new Data Source Adapter and change the config file
- 2.1 modify the method `register` from class `app/Providers/AppServiceProvider.php` 
- 2.2 modify the `config/services.sources` with the desired option for data source

# Contributing

    https://bitbucket.org/viorelrosu/oat-assessment-api.git