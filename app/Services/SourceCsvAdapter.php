<?php

namespace App\Services;

use App\Interfaces\Source;
use App\SourceCsv;

class SourceCsvAdapter implements Source
{

    /**
     * getData - Reads and returns the list of translated questions and associated choices from the storage/app/questions.csv file
     *
     * @param  String $lang - Language (ISO-639-1 code) in which the questions and choices should be translated
     * @return object - The list of translated questions and associated choices
     */
    public function getData(String $lang)
    {
        $csv = new SourceCsv();
        $data = $csv->getDataCsv($lang);
        return $data;
    }

    /**
     * insertData - Inserts a new question and associated choices in the storage/app/questions.csv file
     *
     * @param  Array $data
     * @return object - The question and associated choices (not translated) inserted
     */
    public function insertData(Array $data) {
        $csv = new SourceCsv();
        $dataCsv = $csv->insertDataCsv($data);
        return $dataCsv;
    }
}