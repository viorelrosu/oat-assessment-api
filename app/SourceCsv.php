<?php

namespace App;
use Stichoza\GoogleTranslate\GoogleTranslate;

class SourceCsv
{

    /**
     * getData - Reads and returns the list of translated questions and associated choices from the storage/app/questions.csv file
     *
     * @param  String $lang - Language (ISO-639-1 code) in which the questions and choices should be translated
     * @return object - The list of translated questions and associated choices
     */
    public function getDataCsv(String $lang)
    {
        $tr = new GoogleTranslate();
        $tr->setTarget($lang);

        $data = collect();
        $file = fopen(storage_path("app/questions.csv"), "r");
        while ( ($line = fgetcsv($file, 1000, ",")) !== FALSE )
            {
                if($line[0] == 'Question text') continue;

                $data->push([
                    'text' => $tr->translate($line[0]),
                    'createdAt' => $line[1],
                    'choices' => [ 
                        ['text' => $tr->translate($line[2])],
                        ['text' => $tr->translate($line[3])],
                        ['text' => $tr->translate($line[4])],
                    ],
                ]);
            }
        fclose($file);

        return $data;
    }

    /**
     * insertData - Inserts a new question and associated choices in the storage/app/questions.csv file
     *
     * @param  Array $data
     * @return object - The question and associated choices (not translated) inserted
     */
    public function insertDataCsv(Array $data) {

        $data['createdAt'] = date("Y-m-d h:i:s");

        $file = fopen(storage_path("app/questions.csv"), 'a');
        $line = [];
        $line[] = $data['text'];
        $line[] = $data['createdAt'];
        $line[] = $data['choices'][0]['text'];
        $line[] = $data['choices'][1]['text'];
        $line[] = $data['choices'][2]['text'];
        fputcsv($file, $line);

        return $data;
    }
}