<?php

namespace App\Services;

use App\Interfaces\Source;
use App\SourceJson;

class SourceJsonAdapter implements Source
{    
    /**
     * getData - Reads and returns the list of translated questions and associated choices from the storage/app/questions.json file
     *
     * @param  String $lang - Language (ISO-639-1 code) in which the questions and choices should be translated
     * @return object - The list of translated questions and associated choices
     */
    public function getData(String $lang)
    {
        $json = new SourceJson();
        $data = $json->getDataJson($lang);
        return $data;
    }
    
    /**
     * insertData - Inserts a new question and associated choices in the storage/app/questions.json file
     *
     * @param  Array $data - Question and associated choices to insert
     * @return object - The question and associated choices (not translated) inserted
     */
    public function insertData(Array $data)
    {
        $json = new SourceJson($data);
        $dataJson = $json->insertDataJson($data); 
        return $dataJson;
    }
}
