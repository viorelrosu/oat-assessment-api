<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\Source as SourceInterface;
use App\Services\SourceJsonAdapter;
use App\Services\SourceCsvAdapter;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SourceInterface::class, function ($app) {
            switch ($app->make('config')->get('services.source')) {
                case 'json':
                    return new SourceJsonAdapter;
                case 'csv':
                    return new SourceCsvAdapter;
                default:
                    throw new \RuntimeException("Unknown Source Service");
            }
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
