<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Interfaces\Source as SourceInterface;
use Illuminate\Http\Request;
use Validator;

class QuestionsApiController extends Controller
{
        
    /**
     * getQuestions - Returns the list of translated questions and associated choices
     *
     * @param  Request $request - Includes a lang item which is Language (ISO-639-1 code) in which the questions and choices should be translated
     * @param  SourceInterface $sourceInterface - The Interface Class who handles the source data which is defined in config/services.source
     * @return object - The list of translated questions and associated choices
     * 
     */
    public function getQuestions(Request $request, SourceInterface $sourceInterface)
    {

        try {
            $rules = [
                'lang' => 'required|size:2'
            ];

            $messages = [
                'lang.size' => 'The lang parameter should have Language (ISO-639-1 code) format'
            ];

            $validation = Validator::make($request->all(), $rules, $messages);
            if ($validation->fails()) {
                $message = $validation->errors()->all()[0];
                throw new \Exception($message);
            }

            $data = $sourceInterface->getData($request->get('lang'));

            return response()->json($data, 200);
        } catch (\Exception $ex) {
            return response()->json($ex->getMessage(), 203);
        }
    }
    
    /**
     * createQuestion - Creates a new question and associated choices
     * (the number of associated choices must be exactly equal to 3)
     *
     * @param  Request $request - Includes question object to insert 
     * @param  SourceInterface $sourceInterface - The Interface Class who handles the source data which is defined in config/services.source
     * @return object - The question and associated choices (not translated)
     */
    public function createQuestion(Request $request, SourceInterface $sourceInterface)
    {
        try {
            $rules = [
                'text' => 'required|max:100',
                'choices'=>'required|array|size:3',
                'choices.*.text' => 'required|max:50'
            ];

            $validation = Validator::make($request->all(), $rules);
            if ($validation->fails()) {
                $message = $validation->errors()->all()[0];
                throw new \Exception($message);
            }

            $data = $sourceInterface->insertData($request->all());

            return response()->json($data, 200);
        } catch (\Exception $ex) {
            return response()->json($ex->getMessage(), 203);
        }
    }
}
