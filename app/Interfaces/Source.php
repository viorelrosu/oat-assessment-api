<?php
namespace App\Interfaces;

interface Source {
        
    /**
     * getData - Returns the list of translated questions and associated choices
     *
     * @param  String $lang - Language (ISO-639-1 code) in which the questions and choices should be translated
     */
    public function getData(String $lang);

    /**
     * insertData - Creates a new question and associated choices
     *
     * @param  Array $data - Question object to insert
     */
    public function insertData(Array $data);
}